import ply.lex as lex

states = (
    ('ccomment', 'exclusive'),
    )

# List of keywords. Each keyword will be return as a token of a specific
# type, which makes it easier to match it in grammatical rules.
keywords = {'array': 'ARRAY',
            'break': 'BREAK',
            'do': 'DO',
            'else': 'ELSE',
            'end': 'END',
            'for': 'FOR',
            'function': 'FUNCTION',
            'if': 'IF',
            'in': 'IN',
            'let': 'LET',
            'nil': 'NIL',
            'of': 'OF',
            'then': 'THEN',
            'to': 'TO',
            'type': 'TYPE',
            'var': 'VAR',
            'while': 'WHILE'}

# List of tokens that can be recognized and are handled by the current
# grammar rules.
tokens = ('END', 'IN', 'LET', 'VAR',
          'PLUS', 'TIMES',
          'COMMA', 'SEMICOLON',
          'LPAREN', 'RPAREN',
          'NUMBER', 'ID',
          'COLON', 'ASSIGN','MINUS',
          'DIVIDE','STRICTINFERIOR',
          'STRICTSUPERIOR','INFERIOR',
          'SUPERIOR', 'DIFF', 'EQUAL',
          'AND','OR','IF','THEN','ELSE',
          'FUNCTION','TYPE','WHILE',
          'DO','FOR','TO','BREAK')

t_PLUS = r'\+'
t_MINUS = r'\-'
t_TIMES = r'\*'
t_DIVIDE = r'\/'
t_STRICTINFERIOR = r'\<'
t_STRICTSUPERIOR = r'\>'
t_INFERIOR = r'\<='
t_SUPERIOR = r'\>='
t_DIFF = r'\<>'
t_EQUAL = r'\='
t_AND = r'\&'
t_OR = r'\|'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_COLON = r':'
t_ASSIGN = r':='
t_COMMA = r','
t_SEMICOLON = r';'

t_ignore = ' \t'

# Count lines when newlines are encountered
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

#Commentary
def t_ccomment_end(t):
    r'\*/'
    t.lexer.pop_state()

'''def t_ccomment_content(t):
    r'.|\n'
    pass'''

def t_ccomment_ignore_text(t):
    r'(?!/\*|\*/).'
    pass

def t_ccomment_ignore_newline(t):
    r'\n+'
    pass


def t_ANY_commentstart(t):
    r'/\*'
    t.lexer.push_state('ccomment')

def t_single_line_comment(t):
    r'//.*'
    pass

    
# Distinguish between identifier and keyword. If the keyword is not also
# in the tokens list, this is a syntax error pure and simple since we do
# not know what to do about it.
def t_ID(t):
    r'[A-Za-z][A-Za-z\d_]*'
    if t.value in keywords:
        t.type = keywords.get(t.value)
        if t.type not in tokens:
            raise lex.LexError("unhandled keyword %s" % t.value, t.type)
    return t

# Recognize number - no leading 0 are allowed
def t_NUMBER(t):
    r'[1-9]\d*|0'
    t.value = int(t.value)
    return t

def t_error(t):
    raise lex.LexError("unknown token %s" % t.value, t.value)

lexer = lex.lex()
