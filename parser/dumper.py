from ast.nodes import *
from utils.visitor import *


class Dumper(Visitor):

    def __init__(self, semantics):
        """Initialize a new Dumper visitor. If semantics is True,
        additional information will be printed along with declarations
        and identifiers."""
        self.semantics = semantics

    @visitor(None)
    def visit(self, node):
        raise Exception("unable to dump %s" % node)

    @visitor(IntegerLiteral)
    def visit(self, i):
        return str(i.intValue)

    @visitor(BinaryOperator)
    def visit(self, binop):
        # Always use parentheses to reflect grouping and associativity,
        # even if they may be superfluous.
        return "(%s %s %s)" % \
               (binop.left.accept(self), binop.op, binop.right.accept(self))

    @visitor(Identifier)
    def visit(self, id):
        if id.decl != None :
            if id.decl.type == None:
                diff = id.depth - id.decl.depth
                scope_diff = "/*%d*/" % diff if diff else ''
            else:
                scope_diff = ''
        else:
            scope_diff = ''
        return '%s%s' % (id.name, scope_diff)

    @visitor(IfThenElse)
    def visit(self, ite):
        if ite.else_part == None:
            return "if %s then %s" % \
               (ite.condition.accept(self),ite.then_part.accept(self))
        else:
            return "if %s then %s else %s" % \
               (ite.condition.accept(self),ite.then_part.accept(self),ite.else_part.accept(self))

    @visitor(While)
    def visit(self,wi):
        return "while %s do %s" % \
               (wi.condition.accept(self),wi.exp.accept(self))

    @visitor(IndexDecl)
    def visit(self,id):
        return "%s" % (id.name)
    
    @visitor(For)
    def visit(self,fo):
        return "for %s := %s to %s do %s" %\
            (fo.indexdecl.accept(self),fo.low_bound.accept(self),fo.high_bound.accept(self),fo.exp.accept(self))

    @visitor(Break)
    def visit (self,Br):
        return "break"
    
    @visitor(SeqExp)
    def visit(self, se):
        tamponstring = '('
        if len(se.exps) == 0:
            return "()"
        elif len(se.exps) == 1:
            return "%s" % (se.exps[0].accept(self))
        else:
            for j in range(len(se.exps)-1):
                tamponstring = "%s%s; " % \
                               (tamponstring,se.exps[j].accept(self))
            tamponstring = "%s %s)" % \
                           (tamponstring,se.exps[j+1].accept(self))
            return "%s"% (tamponstring)

    @visitor(Assignment)
    def visit(self,asgn):
        if asgn.identifier.decl != None :
            if asgn.identifier.decl.type == None :
                scope_diff = ''
            else:
                diff = asgn.identifier.depth - asgn.identifier.decl.depth
                scope_diff = "/*%d*/" % diff if diff else ''
                
        if asgn.identifier.decl == None :
            return "%s := %s" % (asgn.identifier.accept(self),asgn.exp.accept(self))
        else:
            if asgn.identifier.decl.type == None :
                return "%s%s := %s" % (asgn.identifier.accept(self),scope_diff,asgn.exp.accept(self))
            else :
                return "%s := %s" % (asgn.identifier.accept(self),asgn.exp.accept(self))
    
        
    @visitor(VarDecl)
    def visit(self, vd):
        if vd.escapes:
            escape ='/*e*/'
        else:
            escape = ''
        if vd.type == None :
            return "var %s%s := %s" % (vd.name,escape,vd.exp.accept(self))
        elif vd.exp == None :
            return "%s%s: %s" % (vd.name,escape,vd.type.typename)
        else:
            return "var %s: %s := %s" % \
              (vd.name,vd.type.typename,vd.exp.accept(self))
    
    @visitor(FunDecl)
    def visit(self, fd):
        tamponstring =''
        a = len(fd.args)
        if len(fd.args) == 0:
            if fd.type == None:
                return "function %s() = %s" % \
          (fd.name,fd.exp.accept(self))
            elif fd.type.typename == "void":
                return "function %s() = %s" % \
          (fd.name,fd.exp.accept(self))
            else:
                return "function %s(): %s = %s" % \
          (fd.name,fd.type.typename,fd.exp.accept(self))
        elif len(fd.args) == 1:
             if fd.type == None:
                 return "function %s(%s) = %s" % \
          (fd.name,fd.args[0].accept(self),fd.exp.accept(self))
             else:
                return "function %s(%s): %s = %s " % \
          (fd.name,fd.args[0].accept(self),fd.type.typename,fd.exp.accept(self))
        else:
            for i in range(len(fd.args)-1):
                tamponstring = '%s%s, ' % (tamponstring, fd.args[i].accept(self))
            tamponstring = '%s%s' % (tamponstring, fd.args[i+1].accept(self))
            if fd.type == None:
                return "function %s(%s) = %s " % \
          (fd.name,tamponstring,fd.exp.accept(self))
            else:
                return "function %s(%s): %s = %s " % \
          (fd.name,tamponstring,fd.type.typename,fd.exp.accept(self))
        
    @visitor(FunCall)
    def visit(self, fc):
        tamponstring =''
        if len(fc.params) == 0:
            return "%s()" % (fc.identifier.name)
        elif len(fc.params) == 1:
            return "%s(%s)" % (fc.identifier.name,fc.params[0].accept(self))
        else:
            for i in range(len(fc.params)-1):
                tamponstring = '%s%s,' % (tamponstring, fc.params[i].accept(self))
            tamponstring = '%s%s' % (tamponstring, fc.params[i+1].accept(self))
                                            
            return "%s(%s)" % (fc.identifier.name,tamponstring)

    @visitor(Let)
    def visit(self, let):
        tamponstring1 = ''
        tamponstring2 = ''
        if (len(let.decls) == 1):
            tamponstring1 = '%s' % let.decls[0].accept(self)
        else:
            for i in range(len(let.decls)-1):
                tamponstring1 = tamponstring1 + '%s ' % let.decls[i].accept(self)
            tamponstring1 = tamponstring1 + '%s' % let.decls[i+1].accept(self)
        tamponstring2 = ''
        if (len(let.exps) == 0):
            tamponstring2 = ''
        elif (len(let.exps) == 1):
            tamponstring2 = '%s' % let.exps[0].accept(self)
        else:
            for i in range(len(let.exps)-1):
                tamponstring2 = tamponstring2 + '%s; ' % let.exps[i].accept(self)
            tamponstring2 = tamponstring2 + '%s' % let.exps[i+1].accept(self)
        return "let %s in %s end" % \
          (tamponstring1,tamponstring2)
          
