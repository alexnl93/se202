from ast.nodes import *
from utils.visitor import visitor
from math import *

class Evaluator:
    """This contains a simple evaluator visitor which computes the value
    of a tiger expression."""

    @visitor(IntegerLiteral)
    def visit(self, int):
        return int.intValue

    @visitor(BinaryOperator)
    def visit(self, binop):
        left = binop.left.accept(self)
        op = binop.op
        if op == '+':
            right = binop.right.accept(self)
            return left + right
        elif op == '-':
            right = binop.right.accept(self)
            return left - right
        elif op == '*':
            right = binop.right.accept(self)
            return left * right
        elif op == '/':
            right = binop.right.accept(self)
            return floor(left / right)
        elif op == '<':
            right = binop.right.accept(self)
            return left < right
        elif op == '<=':
            right = binop.right.accept(self)
            return left <= right
        elif op == '>':
            right = binop.right.accept(self)
            return left > right
        elif op == '>=':
            right = binop.right.accept(self)
            return left >= right
        elif op == '&':
            if left != 0 :
                right = binop.right.accept(self)
                if right != 0:
                    return 1
                else :
                    return 0
            else :
                return 0
        elif op == '|':
            if left != 0 :
                return 1
            else:
                right = binop.right.accept(self)
                if right != 0:
                    return 1
                else:
                    return 0
        elif op == '<>':
            right = binop.right.accept(self)
            if left != right:
                return 1
            else:
                return 0
        elif op == '=':
            right = binop.right.accept(self)
            if left == right:
                return 1
            else:
                return 0
        else:
            raise SyntaxError("unknown operator %s" % op)

    @visitor(SeqExp)
    def visit(self, se):
        for i in se.exps:
            i.accept(self)
        return i.accept(self)
        
    @visitor(IfThenElse)
    def visit(self, ite):
        condition = ite.condition.accept(self)
        if condition == 1:
            then_part = ite.then_part.accept(self)
            return then_part
        else:
            else_part = ite.else_part.accept(self)
            return else_part

    @visitor(None) 
    def visit(self, node):
        raise SyntaxError("no evaluation defined for %s" % node)
