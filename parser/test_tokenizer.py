import unittest

from ply.lex import LexError

from .tokenizer import lexer

class TestLexer(unittest.TestCase):

    def check(self, type, value):
        t = lexer.token()
        self.assertEqual(t.type, type)
        self.assertEqual(t.value, value)

    def check_end(self):
        t = lexer.token()
        self.assertIsNone(t)

    def test_basic(self):
        lexer.input("42")
        self.check('NUMBER', 42)
        self.check_end()

    def test_op(self):
        lexer.input("1 + 2 * 3")
        self.check('NUMBER', 1)
        self.check('PLUS', '+')
        self.check('NUMBER', 2)
        self.check('TIMES', '*')
        self.check('NUMBER', 3)
        self.check_end()

    def test_ite1(self):
        lexer.input("if 1 then 30 else 20")
        self.check('IF', 'if')
        self.check('NUMBER', 1)
        self.check('THEN', 'then')
        self.check('NUMBER', 30)
        self.check('ELSE', 'else')
        self.check('NUMBER', 20)
        self.check_end()

    def test_type(self):
        lexer.input("a: int")
        self.check('ID', 'a')
        self.check('COLON', ':')
        self.check('ID', 'int')

    def test_vardecl(self):
        lexer.input("var a: int := 3")
        self.check('VAR', 'var')
        self.check('ID', 'a')
        self.check('COLON', ':')
        self.check('ID', 'int')
        self.check('ASSIGN', ':=')
        self.check('NUMBER', 3)

    def test_let(self):
        lexer.input("let 1 in 1 end")
        self.check('LET', 'let')
        self.check('NUMBER', 1)
        self.check('IN', 'in')
        self.check('NUMBER', 1)
        self.check('END', 'end')

    def test_fundecl(self):
        lexer.input("function add(x: int, y: int): int = x + y")
        self.check('FUNCTION','function')
        self.check('ID', 'add')
        self.check('LPAREN', '(')
        self.check('ID', 'x')
        self.check('COLON', ':')
        self.check('ID', 'int')
        self.check('COMMA',',')
        self.check('ID', 'y')
        self.check('COLON', ':')
        self.check('ID', 'int')
        self.check('RPAREN', ')')
        self.check('COLON', ':')
        self.check('ID', 'int')
        self.check('EQUAL', '=')
        self.check('ID', 'x')
        self.check('PLUS', '+')
        self.check('ID', 'y')
        
    def test_keyword(self):
        lexer.input("var")
        self.check('VAR', 'var')
        self.check_end()

    def test_identifier(self):
        lexer.input("foobar")
        self.check('ID', 'foobar')
        self.check_end()

    def test_error(self):
        lexer.input("foobar@")
        self.check('ID', 'foobar')
        self.assertRaises(LexError, lexer.token)

    def test_unhandled_keyword(self):
        lexer.input("array")
        self.assertRaises(LexError, lexer.token)

if __name__ == '__main__':
    unittest.main()
