from ast.nodes import *
from utils.visitor import *


class BindException(Exception):
    """Exception encountered during the binding phase."""
    pass


class Binder(Visitor):
    """The binder takes care of linking identifier uses to its declaration. If
    will also remember the depth of every declaration and every identifier,
    and mark a declaration as escaping if it is accessed from a greater depth
    than its definition.

    A new scope is pushed every time a let, function declaration or for loop is
    encountered. It is not allowed to have the same name present several
    times in the same scope.

    The depth is increased every time a function declaration is encountered,
    and restored afterwards.

    A loop node for break is pushed every time we start a for or while loop.
    Pushing None means that we are outside of break scope, which happens in the
    declarations part of a let."""

    def __init__(self):
        """Create a new binder with an initial scope for top-level
        declarations."""
        self.depth = 0
        self.scopes = []
        self.push_new_scope()
        self.break_stack = [None]

    def push_new_scope(self):
        """Push a new scope on the scopes stack."""
        self.scopes.append({})

    def pop_scope(self):
        """Pop a scope from the scopes stack."""
        del self.scopes[-1]

    def current_scope(self):
        """Return the current scope."""
        return self.scopes[-1]

    def push_new_loop(self, loop):
        """Push a new loop node on the break stack."""
        self.break_stack.append(loop)

    def pop_loop(self):
        """Pop a loop node from the break stack."""
        del self.break_stack[-1]

    def current_loop(self):
        loop = self.break_stack[-1]
        if loop is None:
            raise BindException("break called outside of loop")
        return loop

    def add_binding(self, decl):
        """Add a binding to the current scope and set the depth for
        this declaration. If the name already exists, an exception
        will be raised."""
        if decl.name in self.current_scope():
            raise BindException("name already defined in scope: %s" %
                                decl.name)
        self.current_scope()[decl.name] = decl
        decl.depth = self.depth

    def lookup(self, identifier):
        """Return the declaration associated with a identifier, looking
        into the closest scope first. If no declaration is found,
        raise an exception. If it is found, the decl and depth field
        for this identifier are set, and the escapes field of the
        declaration is updated if needed."""
        name = identifier.name
        for scope in reversed(self.scopes):
            if name in scope:
                decl = scope[name]
                identifier.decl = decl
                identifier.depth = self.depth
                decl.escapes |= self.depth > decl.depth
                return decl
        else:
            raise BindException("name not found: %s" % name)

    @visitor(None)
    def visit(self,node):
        self.visit_all(node.children)
        
    @visitor(Identifier)
    def visit(self,id):
        self.lookup(id)

    @visitor(For)
    def visit(self,fo):
        self.push_new_loop(fo)
        fo.low_bound.accept(self)
        fo.high_bound.accept(self)
        str1 = "%s" % (fo.low_bound)
        str2 = "%s" % (fo.high_bound)
        if str1[1] == "d":
            if isinstance(self.lookup(fo.low_bound),IndexDecl):
                raise BindException("No way to set the for variable with the dor variable")
        if str2[1] == "d":
            if isinstance(self.lookup(fo.high_bound),IndexDecl):
                raise BindException("No way to set the for variable with the dor variable")
        self.push_new_scope()
        self.add_binding(fo.indexdecl)
        fo.exp.accept(self)
        self.pop_scope()
        self.pop_loop()

    @visitor(While)
    def visit(self, wh):
        self.push_new_loop(wh)
        for i in wh.children:
            i.accept(self)
        self.pop_loop()
        
    @visitor(Break)
    def visit(self,br):
        self.current_loop()
        self.pop_loop()        
        
    @visitor(VarDecl)
    def visit(self,vd):
        if isinstance(vd.exp,SeqExp):
            if len(vd.exp.exps) > 1:
                raise BindException("Impossible to assign several expression in one")
            else:
                for i in vd.children:
                    i.accept(self)
        for i in vd.children:
            i.accept(self)
        self.add_binding(vd)
        
    @visitor(FunDecl)
    def visit(self,fd):
        self.add_binding(fd)
        self.push_new_scope()
        self.depth += 1
        for i in fd.children:
            i.accept(self)
        self.depth -= 1
        self.pop_scope()        

    @visitor(FunCall)
    def visit(self,fc):
        if isinstance(self.lookup(fc.identifier),FunDecl):
            if (len(self.lookup(fc.identifier).args) == len(fc.params)):
                for i in fc.params:
                    i.accept(self)
            else:
                raise BindException("Function args different from function params")
        else:
            raise BindException("Fuction is not declared")

    @visitor(Assignment)
    def visit(self, asgn):
        if isinstance(self.lookup(asgn.identifier),FunDecl):
            raise BindException("This is a function an not a var")
        if isinstance(self.lookup(asgn.identifier),IndexDecl):
            raise BindException("Impossible to assigne an index loop")
        if isinstance(asgn.exp,SeqExp):
            if len(asgn.exp.exps) > 1:
                raise BindException("Impossible to assign several expression in one")
            else:
                for i in asgn.children:
                    i.accept(self)
        for i in asgn.children:
            i.accept(self)
        
    @visitor(Let)
    def visit(self,let):
        self.push_new_scope()
        for i in let.decls:
            i.accept(self)
        for j in let.exps:
            if isinstance(j,VarDecl):
                if j.name in self.current_scope():
                    for k in j.children:
                        k.accept(self)
                else:
                    raise BindException("Variable %s is not declared in decl part", j.name)
            else:
                j.accept(self)
        self.pop_scope()
