from ast.nodes import *
from . import tokenizer
import ply.yacc as yacc

tokens = tokenizer.tokens

precedence = (
     
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'EQUAL','DIFF'),
    ('left', 'SUPERIOR','INFERIOR','STRICTINFERIOR','STRICTSUPERIOR'),
    ('left', 'MINUS','PLUS'),
    ('left', 'TIMES','DIVIDE'),
    ('right', 'UMINUS')

)

def p_expression_uminus(p):
    'expression : MINUS expression %prec UMINUS'
    p[0] = BinaryOperator(p[1], IntegerLiteral(0), p[2])


def p_expression_binop(p):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression INFERIOR expression
                  | expression SUPERIOR expression
                  | expression STRICTINFERIOR expression
                  | expression STRICTSUPERIOR expression
                  | expression EQUAL expression
                  | expression OR expression
                  | expression AND expression
                  | expression DIFF expression
                  | LPAREN expression PLUS expression RPAREN
                  | LPAREN expression MINUS expression RPAREN
                  | LPAREN expression TIMES expression RPAREN
                  | LPAREN expression DIVIDE expression RPAREN
                  | LPAREN expression INFERIOR expression RPAREN
                  | LPAREN expression SUPERIOR expression RPAREN
                  | LPAREN expression STRICTINFERIOR expression RPAREN
                  | LPAREN expression STRICTSUPERIOR expression RPAREN
                  | LPAREN expression EQUAL expression RPAREN
                  | LPAREN expression OR expression RPAREN
                  | LPAREN expression AND expression RPAREN
                  | LPAREN expression DIFF expression RPAREN'''
    
    if len(p) == 4:
        p[0] = BinaryOperator(p[2], p[1], p[3])
    else:
        p[0] = BinaryOperator(p[3], p[2], p[4])

def p_expression_ite(p):
    '''expression : IF expression THEN expression 
                  | IF expression THEN expression ELSE expression
                  | LPAREN IF expression THEN expression RPAREN
                  | LPAREN IF expression THEN expression ELSE expression RPAREN'''
                  
                  
    if len(p) == 5:
        p[0] = IfThenElse(p[2],p[4],None)
    elif len(p) == 7:
        if p[1] == 'if':
            p[0] = IfThenElse(p[2],p[4],p[6])
        else:
            p[0] = IfThenElse(p[3],p[5],None)
    else:
        p[0] = IfThenElse(p[3],p[5],p[7])

def p_expression_while(p):
    'expression : WHILE expression DO expression'
    p[0] = While(p[2],p[4])

def p_for(p):
    'expression : FOR ID ASSIGN expression TO expression DO expression'
    p[0] = For(IndexDecl(p[2]), p[4], p[6], p[8])

def p_expression(p):
    'expression : BREAK'
    p[0] = Break()

    
###########Bloc seq_exp###################
def p_expression_seqExp(p):
    'expression : LPAREN exps RPAREN'
    p[0] = SeqExp(p[2])

def p_exp(p):
    '''exps :                                        
            | expressions'''
    p[0] = p[1] if len(p) == 2 else []
    
def p_exps(p):
    '''expressions : expression                                     
                   | expressions SEMICOLON expression'''
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]
    
##########################################

def p_expression_number(p):
    'expression : NUMBER'
    p[0] = IntegerLiteral(p[1])

def p_expression_identifier(p):
    'expression : ID'
    p[0] = Identifier(p[1])

###########Bloc let#######################
    
def p_expression_let(p):
    '''expression : LET decls IN letExps END
                  | LET decls IN END'''
    p[0] = Let(p[2],p[4]) if len(p) == 6 else Let(p[2],[]) 

#set aux expressions du let qui comprends la réasignation de variable

def p_letExp(p):
    '''letExp :                                        
              | expression'''
    p[0] = p[1] if len(p) == 2 else []
    
def p_letExps(p):
    '''letExps : letExp                                     
               | letExps SEMICOLON letExp'''
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]

def p_expression_assignment(p):
    'expression : ID ASSIGN expression'
    p[0] = Assignment(Identifier(p[1]),p[3])
    
#sert aux déclarations du let 
def p_decl(p):
    '''decl : VAR ID ASSIGN expression                                      
            | VAR ID COLON ID ASSIGN expression  
            | FUNCTION ID LPAREN  RPAREN EQUAL expression
            | FUNCTION ID LPAREN fundecl_args RPAREN EQUAL expression
            | FUNCTION ID LPAREN  RPAREN COLON ID EQUAL expression
            | FUNCTION ID LPAREN fundecl_args RPAREN COLON ID EQUAL expression '''

    if len(p) == 5:
        p[0] = VarDecl(p[2],None,p[4])
    elif len(p) == 7:
        if p[1] == 'var':
            p[0] = VarDecl(p[2],Type(p[4]),p[6])
        else:
            p[0] = FunDecl(p[2],[],None,p[6])
    elif len(p) == 8:
        p[0] = FunDecl(p[2],p[4],None,p[7])
    elif len(p) == 9:
        p[0] = FunDecl(p[2],[],Type(p[6]),p[8])
    else:
        p[0] = FunDecl(p[2],p[4],Type(p[7]),p[9])
        
        
    
#sert à let    
def p_decls(p):
    '''decls : decl
             | decls decl'''
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[2]]

# dans une declaration de fonction le type est obligatoire        
def p_typed_var(p):
    'typed_var : ID COLON ID'
    p[0] = VarDecl(p[1],Type(p[3]),None)

# liste de la déclaration des paramètres de fonctions
def p_fundecl_args(p):
    '''fundecl_args : typed_var
                    | fundecl_args COMMA typed_var'''
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]
    
###########Bloc funcall#######################
            
def p_expression_funcall(p):
    '''expression : ID LPAREN params RPAREN
                  | ID LPAREN RPAREN'''
    if len(p) == 5:
        p[0] = FunCall(Identifier(p[1]),p[3])
    else:
        p[0] = FunCall(Identifier(p[1]),[])

#sert à funcall c'est la liste d'arguments
def p_params(p):
    '''params : expression
               | params COMMA expression '''
    p[0] = [p[1]] if len(p) == 2 else p[1] + [p[3]]
    
    
def p_error(p):
    import sys
    sys.stderr.write("no way to analyze %s\n" % p)
    sys.exit(1)

parser = yacc.yacc()

def parse(text):
    return parser.parse(text, lexer = tokenizer.lexer.clone())

