import unittest

from parser.dumper import Dumper
from parser.parser import parse

class TestDumper(unittest.TestCase):

    def parse_dump(self, text):
        tree = parse(text)
        return tree.accept(Dumper(semantics=False))

    def check(self, text, expected):
        self.assertEqual(self.parse_dump(text), expected)

    def test_literal(self):
         self.check("42", "42")

    def test_priority(self):
         self.check("1+2*3", "(1 + (2 * 3))")
         self.check("2*3+1", "((2 * 3) + 1)")
         self.check("10-2*3", "(10 - (2 * 3))")
         self.check("2*3-1", "((2 * 3) - 1)")
         self.check("10-6/3", "(10 - (6 / 3))")
         self.check("6/3-1", "((6 / 3) - 1)")
         self.check("if 1 then 100 else 200 + 300","if 1 then 100 else (200 + 300)")
         self.check("if 1 then 100 + 300  else 200","if 1 then (100 + 300) else 200")
         '''self.check("let var a : int := 1 + 2* 3 in a end", "let var a : int := (1 + (2 * 3)) in a end")
         self.check("let var a : int := 1 + 2* 3 var b : int := 2 * 3 in a end", "let var a : int := (1 + (2 * 3)) var b : int := (2 * 3) in a end")
         self.check("let function fig(): int = 10 + 2 in 3 end","let function fig(): int = (10 + 2) in 3 end")
         self.check("let function f() = 10 in 1 end", "let function f() = 10 in 1 end")
         self.check("let function f(a: int) = 1 in 1 end", "let function f(a: int) = 1 in 1 end")
         self.check("let function f(a: int, b: int, c: int) = 1 in 1 end","let function f(a: int, b: int, c: int) = 1 in 1 end")
         self.check("let function f() = f() in f() end", "let function f() = f() in f() end")'''
        
if __name__ == '__main__':
    unittest.main()
