import unittest

from ast.evaluator import Evaluator
from ast.nodes import IntegerLiteral, BinaryOperator, IfThenElse
from parser.parser import parse

class TestEvaluator(unittest.TestCase):

    def check(self, ast, expected):
        self.assertEqual(ast.accept(Evaluator()), expected)

    def parse_check(self, str, expected):
        self.assertEqual(parse(str).accept(Evaluator()), expected)

    def test_literal(self):
        self.check(IntegerLiteral(42), 42)

    def test_basic_operator(self):
        self.check(BinaryOperator('+', IntegerLiteral(10), IntegerLiteral(20)), 30)
        self.check(BinaryOperator('-', IntegerLiteral(50), IntegerLiteral(20)), 30)
        self.check(BinaryOperator('/', IntegerLiteral(50), IntegerLiteral(5)), 10)
        self.check(BinaryOperator('<', IntegerLiteral(50), IntegerLiteral(50)), 0)
        self.check(BinaryOperator('>', IntegerLiteral(50), IntegerLiteral(50)), 0)
        self.check(BinaryOperator('<', IntegerLiteral(5), IntegerLiteral(50)), 1)
        self.check(BinaryOperator('<', IntegerLiteral(50), IntegerLiteral(5)), 0)
        self.check(BinaryOperator('>', IntegerLiteral(50), IntegerLiteral(5)), 1)
        self.check(BinaryOperator('>', IntegerLiteral(5), IntegerLiteral(50)), 0)
        self.check(BinaryOperator('<=', IntegerLiteral(50), IntegerLiteral(50)), 1)
        self.check(BinaryOperator('>=', IntegerLiteral(50), IntegerLiteral(50)), 1)
        self.check(BinaryOperator('<=', IntegerLiteral(5), IntegerLiteral(50)), 1)
        self.check(BinaryOperator('<=', IntegerLiteral(50), IntegerLiteral(5)), 0)
        self.check(BinaryOperator('>=', IntegerLiteral(50), IntegerLiteral(5)), 1)
        self.check(BinaryOperator('>=', IntegerLiteral(5), IntegerLiteral(50)), 0)
        self.check(BinaryOperator('&', IntegerLiteral(6), IntegerLiteral(0)), 0)
        self.check(BinaryOperator('&', IntegerLiteral(0), IntegerLiteral(0)), 0)
        self.check(BinaryOperator('&', IntegerLiteral(6)  , IntegerLiteral(8)), 1)
        self.check(BinaryOperator('|', IntegerLiteral(6), IntegerLiteral(0)), 1)
        self.check(BinaryOperator('|', IntegerLiteral(0), IntegerLiteral(0)), 0)
        self.check(BinaryOperator('|', IntegerLiteral(6), IntegerLiteral(8)), 1)
        self.check(BinaryOperator('<>', IntegerLiteral(0), IntegerLiteral(0)), 0)
        self.check(BinaryOperator('<>', IntegerLiteral(6), IntegerLiteral(8)), 1)
        self.check(BinaryOperator('<>', IntegerLiteral(6), IntegerLiteral(6)), 0)
        self.check(BinaryOperator('=', IntegerLiteral(0), IntegerLiteral(0)), 1)
        self.check(BinaryOperator('=', IntegerLiteral(6), IntegerLiteral(8)), 0)
        self.check(BinaryOperator('=', IntegerLiteral(6), IntegerLiteral(6)), 1)       

    def test_priorities(self):
        self.check(BinaryOperator('+', IntegerLiteral(1), BinaryOperator('*', IntegerLiteral(2), IntegerLiteral(3))), 7)
        self.check(BinaryOperator('-', IntegerLiteral(10), BinaryOperator('*', IntegerLiteral(2), IntegerLiteral(3))), 4)
        
    def test_parse_literal(self):
        self.parse_check('42', 42)

    def test_parse_sequence(self):
        self.parse_check('1+(2+3)',6)
        self.parse_check('1+(2+3)+4', 10)

    def test_precedence(self):
        self.parse_check('1 + 2 * 3', 7)
        self.parse_check('2 * 3 + 1', 7)

    def test_condition(self):
        self.check(IfThenElse(IntegerLiteral(0),IntegerLiteral(19),IntegerLiteral(21)), 21)
        self.check(IfThenElse(IntegerLiteral(1),IntegerLiteral(19),IntegerLiteral(21)), 19)
        self.parse_check('if 1 then 5 else 9', 5)
        self.parse_check('if 0 then 5 else 9', 9)
        
if __name__ == '__main__':
    unittest.main()
